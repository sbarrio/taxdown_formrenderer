/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import setup from "../src/setup";


it('renders correctly', () => {

  const AppComponent = setup();
  const app = renderer.create(<AppComponent/>).toJSON();

  expect(app).toMatchSnapshot();    

});
