import { createStore } from 'redux';
import { submissionReducer } from './reducers/submissions';


// Connect our store to the reducers
export const store = createStore(submissionReducer);