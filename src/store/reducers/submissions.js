export const SUBMISSIONS_UPDATED = "SUBMISSIONS_UPDATED";
export const FORM_FIELDS_UPDATED = "FORM_FIELDS_UPDATED";
import TestFormJSON from '../../../res/testForm.json';

let submissionState = { submissions : [], formFields : TestFormJSON.inputFields}

export const submissionReducer = (state = submissionState, action) => {

    switch (action.type) {
        case SUBMISSIONS_UPDATED:
            return {
                ...state,
                submissions : action.submissions
            }
        case FORM_FIELDS_UPDATED :
            return {
                ...state,
                formFields : action.formFields
            }
        default:
            return state;
    }

}