import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MenuScreen from '../screen/MenuScreen';
import FormScreen from '../screen/FormScreen';
import SubmissionListScreen from '../screen/SubmissionListScreen';


export const StackNavigator = createStackNavigator ({
        MenuScreen              :     {screen: MenuScreen, headerMode: 'screen', },
        FormScreen              :     {screen: FormScreen, headerMode: 'screen', },
        SubmissionListScreen    :     {screen: SubmissionListScreen, headerMode: 'screen', }
    }, {
        initialRouteName : "MenuScreen",
    }
  );
  
export const App = createAppContainer(StackNavigator);