import React, { useState, useEffect } from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
} from 'react-native';

const BlueButton = (props) => {

    return (

        <TouchableOpacity 
            style={styles.container}
            onPress={() => props.onPress()}
        >

            <Text style={styles.label}>
                {props.title}
            </Text>

        </TouchableOpacity>

    );

}

export default BlueButton;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    container : {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor : '#5588DD',
        maxHeight : 60,
        minWidth: 200,
        borderRadius: 5,
    },
    label : {
        color : "#FDFDFD",
        fontSize : 20,
    }
})