import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
} from 'react-native';

const MAX_LENGTH_TEXTFIELD = 150;

const FormFieldInput = (props) => {

    const [fieldValue, setFieldValue] = useState(null);

    let field = props.field;
    let keyboardType = "default";  // Default for normal text keyboard

    if (field.type) {

        switch(field.type) {
            case "number":
                keyboardType = "numeric";
                break;
            case "phone":
                keyboardType = "phone-pad";
                break;
            case "decimal":
                keyboardType = "decimal-pad";
                break;
            case "email":
                keyboardType = "email-address";
                break;
            default: 
                keyboardType = "default";
        }
    }

    return (

        <View style={styles.inputHolder}>

            <Text style={styles.inputLabel}>
                {field.label ? field.label : "Field"}
            </Text>

            <TextInput 
                style={styles.textInput}
                placeHolder={field.placeHolder ? field.placeHolder : ""}
                placeholderTextColor="#FFFFFF"
                maxLength={field.placeHolder ? field.placeHolder : MAX_LENGTH_TEXTFIELD}
                key={field.id}
                keyboardType={keyboardType}
                value={fieldValue}
                onChangeText={(value) => {
                        setFieldValue(value)
                        props.updateCallback(field.id, value);
                    }
                }
            ></TextInput>

        </View>

    );

}

export default FormFieldInput;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    inputHolder: {
        minHeight : 65,
        flexDirection: 'row',
        alignItems : 'center',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 30,
        marginTop: 10,
        marginBottom:10,
    },
    inputLabel : {
        flex: 2,
        textAlign: 'right',
        fontSize: 13,
        color: "#0C0C0C",
        paddingRight: 10,
    },
    textInput: {
        flex: 8,
        height: 60,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor : "#EFEFEF",
        color: "#0088EE"
    }
})