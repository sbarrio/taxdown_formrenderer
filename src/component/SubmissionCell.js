import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
} from 'react-native';

const window = Dimensions.get("window");
const DeviceHeight = window.height;
const DeviceWidth = window.width;

const SubmissionCell = (props) => {

    console.log(props);

    let keys = Object.keys(props.submission);
    
    return (

        <View style={styles.submissionCell}>

            <Text style={styles.submissionCellTitle}> 
                Submission {props.index +1 }
            </Text>

            {keys.map((key) => {

                return <View style={styles.submissionRow}>

                            <Text style={styles.submissionCellField}>
                                {props.submission[key].label}:
                            </Text>

                            <Text style={styles.submissionCellValue}>
                                {props.submission[key].value}
                            </Text>

                        </View>

             })
            }

        </View>

    );

}

export default SubmissionCell;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    submissionCell : {
        minHeight : 60,
        flexDirection : 'column',
        alignItems: 'center',
        justifyContent : 'center',
        width: DeviceWidth,
        backgroundColor : "#FEFEFE",
        borderBottomColor: "#FAFAFA",
        borderBottomWidth: 1,        
        paddingTop: 20,
        paddingBottom: 20,
    },
    submissionCellTitle : {
        flex: 1,
        color: "#000000",
        marginBottom: 10,
    },
    submissionRow : {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent : 'center',
        paddingRight: 20,
        paddingLeft: 20,
    },
    submissionCellField : {
        flex: 2,
        color: "#000000",
        textAlign: "right",
        paddingRight: 20,
    },
    submissionCellValue : {
        color: "#0088EE",
        flex: 6,
    }
})