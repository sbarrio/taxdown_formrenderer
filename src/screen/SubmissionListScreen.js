import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
} from 'react-native';
import { useSelector } from "react-redux";

import SubmissionCell from '../component/SubmissionCell';
import { FlatList } from 'react-native-gesture-handler';

const window = Dimensions.get("window");
const DeviceHeight = window.height;
const DeviceWidth = window.width;

const SubmissionListScreen = () => {

    const submissions = useSelector(state => state.submissions);

    function renderSubmissionCell(submission, index) {

        return (
            <SubmissionCell index={index} submission={submission} />
        )

    }

    return (

        <View style={styles.container}>

            
            {submissions != null && submissions.length > 0 &&
                <FlatList
                    style = {styles.submissionList}
                    data = {submissions}
                    renderItem = {({ item : submission, index }) => {
                        return renderSubmissionCell(submission, index);
                      }}
                    keyExtractor = { (submission, index) => index.toString()}
                />
            }

        </View>

    )

}

SubmissionListScreen.navigationOptions = {
    title: 'Submissions',
};

export default SubmissionListScreen;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    container: {
        flex : 1,
    },
    submissionList : {
        flex: 1,
        width: DeviceWidth,
        height : DeviceHeight,
    },
})