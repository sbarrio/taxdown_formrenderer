import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    Alert,
    ScrollView,
    Dimensions,
} from 'react-native';
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { SUBMISSIONS_UPDATED, FORM_FIELDS_UPDATED } from '../store/reducers/submissions';

import TestFormJSON from '../../res/testForm.json';

import FormFieldInput from '../component/FormFieldInput';
import BlueButton from '../component/BlueButton';

const window = Dimensions.get("window");
const DeviceHeight = window.height;
const DeviceWidth = window.width;

const FormScreen = ( {navigation}) => {

    // Redux action dispatcher
    const dispatch = useDispatch();

    // State
    const formFields = useSelector(state => state.formFields);
    const submissions = useSelector(state => state.submissions);
    const [formFieldValues, setFormFieldValues] = useState(null);

    // Init 
    useEffect(() => {

        dispatch({
            type: FORM_FIELDS_UPDATED,
            formFields: TestFormJSON.inputFields,
        })

    }, []); //Runs only once after the first render

    useEffect( () => {

        var newFormFieldValues = {};

        for (let f in formFields) {
            newFormFieldValues[formFields[f].id] = {
                label: formFields[f].label,
                value : ""
            };
            
        }

        setFormFieldValues(newFormFieldValues);

    },[formFields]); // Initializes formFieldValues

    // Callbacks

    const fieldUpdated = (fieldId, value) => {

        var newFormFieldValues = formFieldValues;
        newFormFieldValues[fieldId] = {
            label : newFormFieldValues[fieldId].label,
            value : value
        }
        setFormFieldValues(newFormFieldValues);
        
    }

    const submitForm = () => {

        var newSubmissions = submissions;
        var newSubmission = {}

        for (var f in formFieldValues) {

            if (formFieldValues[f].value.length <= 0) {
                showFillFormError()
                return;
            }

            newSubmission[f] = formFieldValues[f];
        }

        newSubmissions = newSubmissions.concat(newSubmission);
    
        dispatch({
            type: SUBMISSIONS_UPDATED,
            submissions: newSubmissions
        })

        showSuccessMeesage();

    }

    // Alert messages
    const showFillFormError = () => {

        Alert.alert(
            'Error',
            'Some fields are empty.',
            [
                {text: 'OK', onPress: () => console.log('OK')}
            ],
            {cancelable: false},
            );

    }

    const showSuccessMeesage = () => {

        Alert.alert(
            'Success',
            'Form submitted!',
            [
                {text: 'Continue', onPress: () => navigation.goBack()},
            ],
            {cancelable: false},
            );

    }
    

    return (

        <View style={styles.container}>

            <ScrollView style= {styles.formFieldHolder}>

                {formFields.map((field) => {
                        return <FormFieldInput field={field} key={field.id} updateCallback={fieldUpdated}/> 
                    })
                }

            </ScrollView>

            <BlueButton 
                title="Submit" 
                onPress={submitForm}
            />

        </View>

    )

}

FormScreen.navigationOptions = {
    title: 'Form',
};

export default FormScreen;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems : "center",
        justifyContent : "flex-start",
        paddingTop: 20,
        paddingBottom: 20,
    },
    formFieldHolder :{
        flex: 6,
        width: DeviceWidth,
    }
})