import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
} from 'react-native';
import BlueButton from '../component/BlueButton';
import "react-native-gesture-handler"; 

const window = Dimensions.get("window");

const MenuScreen = ( {navigation} ) => {

    return (

        <View style={styles.container}>

            <BlueButton
                title = "Fill submission form"
                onPress={() => navigation.navigate("FormScreen")}
            />

            <BlueButton
                title = "View submissions"
                onPress={() => navigation.navigate("SubmissionListScreen")}
            />
    
        </View> 

    );

}

MenuScreen.navigationOptions = {
    title: 'Menu',
};


export default MenuScreen;

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
    container : {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-evenly",
        backgroundColor : '#FFFFFF',
        paddingTop: window.height * 0.3,
        paddingBottom: window.height * 0.3,
    },
})