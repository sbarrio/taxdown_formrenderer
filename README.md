# FormRenderer 1.0

   **Date:** **19/10/19**
   **Author : Sergio Barrio Slocker**

**----------------------------------------------------------**

- Basic Form Rendering and submission test tool.
- To test different forms please edit the json contianed into /res/testForm.json

**Setup**

On the console, browser to the root projecto folder /MTGCards and run:

npm install

Also, to run on iOS, you will need to browse to the /ios forlder and execute: 

pod install

**Running**

- iOS (Simulator)

On the project's root folder run the following:

   react-native run-ios

- iOS (Real hardware)

NOTE: You will need a valid Apple Developer Account set on XCode and selected under the Signing & Capabilites tab on the general project settings in order for this to work.
In case you want to run the app on a real device navigate to the subfolder ios (inside this project) and open up the file FormRenderer.xcworkspace.
Once on XCode, select your target device and press on the run button.
If you want to test a production build go to Product > Scheme > Edit Scheme and change the Build configuration from Debug to Release.
Once this is done you can select your real device on the target selector and press the run button.
After the project has compiled a production version of the app should quickly be installed and launched on your device.

- Android (Emulated and real hardware)

On the project's root folder run the following:

   react-native run-android

NOTE : You will need to have either a developer-enabled Android device connected to your computer or an emulator (AVD or Genymotion virtualized machine, for example) in order for this to run.